# Angular #
Notas de Angular

### ¿Qué puedes hacer con este repositorio? ###

* Descargarlo
* O 
* [Clonarlo](git clone git@bitbucket.org:ksamueloz/angular.git)

### Instalar de herramientas para Angular ###

* Descarga Node Nvm de GitHub
* [Descárgalo](https://github.com/coreybutler/nvm-windows)
* Ver la versión
* ## nvm --version ##
* Listar las versiones que hay
* ## nvm list ##""
* Instalar una versión específica
* ## nvm install (número versión) ##
* Instalar la última estable a momento de la fecha que corre el comando
* ## nvm install latest ##
* Usar una versión específica, ver lista para confirmar el uso
* ## nvm use (Número versión) ##
* Versión de Node
* ## node --version ##
* Versión de Npm
* ## npm -v ##
* Instalar TypeScript
* ## npm install -g typescript ##
* Instalar Angular Cli
* ## npm install -g @angular/cli
* Ver la versión
* ## ng --version ##
* En caso de no salir la versión de Typescript con ng --version se usa:
* ## tsc -v ##
* Angular-Cli se utiliza para generar la estructura de 0 de un proyecto o bien para crear nuevos elementos como componentes, servicios, etc.
